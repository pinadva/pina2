﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

public class BombScript : MonoBehaviour
{
	public int msec_to_detonate = 3000;
	public int BoomPower = 70;

	Animator animator;

	private DateTime triggeredTime = new DateTime();
	private bool trigered = false;

	void Start ()
	{
		animator = GetComponentInChildren<Animator>();
	}
	
	void Update ()
	{
		if (trigered)
		{
			if (DetonationIsReady())
			{
				Boom();
			}
		}
	}

	private void FixedUpdate()
	{
		
	}

	private void Boom()
	{
		animator.SetTrigger("Boom");

		//get all players in radius

		var searchList = Resources.FindObjectsOfTypeAll<PlayerController>().ToList();
		//send damadge to players in raduis
		
		foreach(var item in searchList)
		{
			if(Vector3.Distance(item.transform.position, this.transform.position) < 450)
			{
				if(item.HealthManager != null)
					item.HealthManager.Damadge(70);
			}
		}

		//add force to all objects in radius

		var allObjects = Physics2D.OverlapCircleAll(this.transform.position, 50);
		//Debug.Log(allObjects.Count());
		foreach(var item in allObjects)
		{
			Vector3 vect = (item.transform.position - this.GetComponent<Rigidbody2D>().transform.position).normalized;
			var rb = item.GetComponent<Rigidbody2D>();
			if(rb != null)
				rb.AddForce(vect * 7500, ForceMode2D.Impulse);
		}

		Destroy(this.gameObject);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (trigered)
			return;

		var searchList = Resources.FindObjectsOfTypeAll<PlayerController>().Select(i => i.gameObject).ToList();

		if (searchList.Contains(collision.gameObject))
		{
			triggeredTime = DateTime.Now;
			animator.SetBool("Triggered", true);
			trigered = true;
		}
		else
		{
			animator.SetBool("Triggered", false);
			trigered = false;
		}
	}

	public bool DetonationIsReady()
	{
		double different = (DateTime.Now - triggeredTime).TotalMilliseconds;
		return different > msec_to_detonate;
	}


}
