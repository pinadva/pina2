﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControllerProvider : MonoBehaviour
{
	public OneTextIndicator healthIndicator;
	public OneTextIndicator armorIndicator;
	public OneTextIndicator playerHelpInfo;

	public PlayerController currentPlayer;
	public Camera currentCamera;



	// Use this for initialization
	void Start ()
	{
		GameController.HelathIndicator = healthIndicator;
		GameController.ArmorIndicator = armorIndicator;
		GameController.currentPlayer = currentPlayer;
		GameController.PlayerHelpInfo = playerHelpInfo;
		GameController.currentCamera = currentCamera;

		GameController.ReInitController();
	}
	
	// Update is called once per frame
	void Update ()
	{
		GameController.UpdatePerFrame();
	}
}
