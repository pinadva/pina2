﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using GamePattern.Controllers;
using GamePattern.Interfaces;
using GamePattern.MovementArchitecture;

public class LongChainScript : MonoBehaviour
{
	List<ChainScript> Chains = new List<ChainScript>();

	Vector2 UpCoordinates
	{
		get;
		set;
	}

	Vector2 DownCoordinates
	{
		get;
		set;
	}

	void Awake ()
	{
		Chains = GetComponentsInChildren<ChainScript>().ToList();

		if (Chains.Count != 0)
			Chains = Chains.OrderBy(i => i.transform.name).ToList();
	}

	private void Start()
	{

	}

	void Update ()
	{

	}

	public void OnSomeChainTriggered(PlayerController trigeredPlayer)
	{
		if(trigeredPlayer.playerOpportunities.playerOpportunities.Any(i => i.Sender == this.gameObject))
		{
			return;
		}

		trigeredPlayer.playerOpportunities.AddPlayerOpportunity(
			new LongChainOpportunity(this.gameObject, trigeredPlayer, null));
	}
}

public class LongChainOpportunity : IPlayerOpportunity
{
	public bool IsOnChain = false;

	public PlayerController currentPlayer
	{
		get;
		set;
	}

	public GameObject Sender
	{
		get;
		set;
	}

	public string Description
	{
		get;
		set;
	}

	float oldGravityScale = -1;

	public LongChainOpportunity(GameObject sender, PlayerController playerController, GameObject currentChain)
	{
		this.Sender = sender;
		this.currentPlayer = playerController;
	}

	public void Invoke()
	{
		if (currentPlayer == null)
			throw new System.NullReferenceException("Player is null.");

		IsOnChain = !IsOnChain;

		Debug.Log(IsOnChain ? "Взбираемся..." : "Слезаем с лестницы");

		if (IsOnChain)
		{
			BeginStair();
		}
		else
		{
			EndStair();
		}
	}

	public void OnBeginContact()
	{
		GameController.SetNewPlayerHelpInfo("Press 'E' to go up");
	}

	public void OnEndContact()
	{
		GameController.SetNewPlayerHelpInfo("");

		if (IsOnChain)
		{
			EndStair();
			IsOnChain = !IsOnChain;
		}

	}

	private void BeginStair()
	{
		oldGravityScale = currentPlayer.MoveController.Rigidbody2D.gravityScale;

		currentPlayer.MoveController.Rigidbody2D.gravityScale = 0;

		//currentPlayer.MoveController = new StairMoveController(currentPlayer.MoveController);

	}

	private void EndStair()
	{
		currentPlayer.MoveController = new PlayerDefault_EarthMoveController(currentPlayer.MoveController);
		currentPlayer.MoveController.Rigidbody2D.gravityScale = oldGravityScale;
	}
}

