﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GamePattern.Interfaces;

public class OneTextIndicator : MonoBehaviour, IDataIndicator
{
	Text textHandle;
	private string _currentData;

	public string CurrentData
	{
		get
		{
			return _currentData;
		}
		set
		{
			_currentData = value;
			textHandle.text = _currentData;
		}
	}
	
	void Start ()
	{
		textHandle = this.GetComponent<Text>();
		_currentData = textHandle.text;
	}
}
