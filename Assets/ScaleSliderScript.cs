﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleSliderScript : MonoBehaviour
{
	public Camera workingCamera;

	public Slider workingSlider;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void SliderValueChanged()
	{
		const int cameraMaxValue = 500;
		const int cameraMinValue = 100;
		float newValue = cameraMaxValue - workingSlider.value;
		this.workingCamera.orthographicSize = newValue < cameraMinValue ? workingSlider.minValue : newValue;
	}
}
