﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

using GamePattern.Helpers;
using GamePattern.Interfaces;
using GamePattern.MovementArchitecture;

namespace GamePattern.Controllers
{
	public abstract class EarthMoveController : MoveController
	{
		protected IForcer FallingForcer
		{
			get;
			set;
		}

		protected IForcer OnEarthForcer
		{
			get;
			set;
		}		

		public EarthMoveController(GameObject playerObject, Rigidbody2D player_rb, Animator player_animator)
			: base(playerObject, player_rb, player_animator)
		{
			this.CurrentJumpingData = new JumpingData(true, true, 50000, 500);

			OnEarthForcer = new ImpulseForcer(player_rb);
			FallingForcer = new ImpulseForcer(player_rb, 4000, 30);
		}

		public EarthMoveController(MoveController moveController)
			: base(moveController)
		{
			this.CurrentJumpingData = new JumpingData(true, true, 50000, 2500);

			this.CanMoveDown = false;
			this.CanMoveInFalling = true;
			this.CanFly = false;

			OnEarthForcer = new ImpulseForcer(Rigidbody2D);
			FallingForcer = new ImpulseForcer(Rigidbody2D, 4000, 30);
		}

		public override void Jump()
		{
			if (IsOnGround && CurrentJumpingData.JumpIsReady())
			{
				Rigidbody2D.AddForce(new Vector2(0, CurrentJumpingData.JumpForce));
				Animator.SetTrigger("Jumping");
				CurrentJumpingData.OldJumpTime = new DateTime();
			}
		}

		public override void FixedTick()
		{
			if (IsOnGround)
			{
				OnEarthForcer.Force(CurrentForceVector);
			}
			else if (CanMoveInFalling)
			{
				FallingForcer.Force(CurrentForceVector);
			}
		}		

		protected override void UpdatePlayerMoveState()
		{
			UpdateIsOnGround();

			if (IsOnGround)
			{
				Animator.SetBool("IsFalling", false);
			}

			if (CurrentForceVector.x != 0)
			{
				Animator.SetBool("IsRunning", true);
			}
			else
			{
				Animator.SetBool("IsRunning", false);
			}

			UpdateScale();
		}
	}
}
