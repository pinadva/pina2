﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;
using GamePattern.Controllers;
using GamePattern.Interfaces;
using GamePattern.Helpers;


public static class GameController
{
	//UI
	public static Camera currentCamera;

	public static PlayerController currentPlayer;

	public static OneTextIndicator HelathIndicator;

	public static OneTextIndicator ArmorIndicator;

	public static OneTextIndicator PlayerHelpInfo;

	//playersControlling
	public static List<PlayerController> ControllablePlayers;

	static UserCommandsRouter userCommandsRouter;

	public static void ReInitController()
	{
		//init control keys
		userCommandsRouter = new UserCommandsRouter();
		userCommandsRouter.RegisterMoveKey(KeyCode.A, MoveAction.PLAYER_MOVE_LEFT);
		userCommandsRouter.RegisterMoveKey(KeyCode.W, MoveAction.PLAYER_MOVE_UP);
		userCommandsRouter.RegisterMoveKey(KeyCode.D, MoveAction.PLAYER_MOVE_RIGHT);
		userCommandsRouter.RegisterMoveKey(KeyCode.S, MoveAction.PLAYER_MOVE_DOWN);
		userCommandsRouter.RegisterMoveKey(KeyCode.Space, MoveAction.PLAYER_JUMP);
		userCommandsRouter.RegisterKey(KeyCode.Q, () =>
		{
			if (currentPlayer.BackPack.BackPackItems.Count != 0)
			{
				currentPlayer.BackPack.DropItem(0, currentPlayer.transform.position +
			new Vector3(5 * currentPlayer.transform.localScale.x, 0, 0));				
			}
		}, KeyEventType.DOWN);
		userCommandsRouter.RegisterKey(KeyCode.E, () =>
		{
			if (currentPlayer != null && currentPlayer.playerOpportunities.playerOpportunities.Count > 0)
			{
				currentPlayer.playerOpportunities.InvokeOpportunity(0);
			}
			else
			{
				Debug.Log("There is nothing to invoking.");
			}
		}, KeyEventType.DOWN);

		if (currentPlayer == null)
		{
			ControllablePlayers = Resources.FindObjectsOfTypeAll(typeof(PlayerController)).
				Select(i => (PlayerController)i).ToList();

			currentPlayer = ControllablePlayers[0];
		}

		currentPlayer.HealthManager.healthChanged = (i) => SetNewCurrentHealth(i);
		currentPlayer.HealthManager.onDeath = () => OnCurrentPlayerDeath();
	}

	public static void UpdatePerFrame()
	{
		//Check for new keys events
		HandleKeys();

		//move camera
		UpdateCameraPosition();
	}

	private static void OnCurrentPlayerDeath()
	{

	}

	private static void SetNewCurrentHealth(int new_health)
	{
		HelathIndicator.CurrentData = new_health.ToString();
	}

	public static void SetNewPlayerHelpInfo(string caption)
	{
		PlayerHelpInfo.CurrentData = caption;
	}

	private static void HandleKeys()
	{
		foreach(var key in userCommandsRouter.KeyDownDictionary.Keys)
		{
			if (Input.GetKeyDown(key))
			{
				userCommandsRouter.InvokeKeyDown(key);
			}
		}

		foreach (var key in userCommandsRouter.KeyUpDictionary.Keys)
		{
			if (Input.GetKeyUp(key))
			{
				userCommandsRouter.InvokeKeyUp(key);
			}
		}

		foreach (var key in userCommandsRouter.KeyPressedDictionary.Keys)
		{
			if (Input.GetKey(key))
			{
				userCommandsRouter.InvokeKeyPressed(key);
			}
		}
	}

	private static void UpdateCameraPosition()
	{
		if (currentCamera != null && currentPlayer != null)
		{
			currentCamera.transform.position =
				new Vector3(currentPlayer.transform.position.x,
				currentPlayer.transform.position.y, currentCamera.transform.position.z);
		}
	}
}
