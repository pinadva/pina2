﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

using GamePattern.Helpers;
using GamePattern.Interfaces;
using GamePattern.MovementArchitecture;

namespace GamePattern.Interfaces
{
	public abstract class MoveController
	{
		public float GravityScale
		{
			get
			{
				return this.Rigidbody2D.gravityScale;
			}
			private set
			{
				this.Rigidbody2D.gravityScale = value;
			}
		}

		public GameObject GameObject
		{
			get;
			set;
		}

		public Transform Transform
		{
			get;
			set;
		}

		public Animator Animator
		{
			get;
			set;
		}

		public Collider2D Collider2D
		{
			get;
			set;
		}

		public Rigidbody2D Rigidbody2D
		{
			get;
			set;
		}

		protected bool _movingLeft = false;
		public bool MovingLeft
		{
			get
			{
				return _movingLeft;
			}
			set
			{
				if (_movingLeft != value)
				{
					_movingLeft = value;
					UpdateForceVector();
					UpdatePlayerMoveState();
					UpdateScale();
				}
			}
		}

		protected bool _movingRight = false;
		public bool MovingRight
		{
			get
			{
				return _movingRight;
			}
			set
			{
				if (_movingRight != value)
				{
					_movingRight = value;
					UpdateForceVector();
					UpdatePlayerMoveState();
					UpdateScale();
				}
			}
		}

		protected bool _movingUp = false;
		public bool MovingUp
		{
			get
			{
				return _movingUp;
			}
			set
			{
				if (_movingUp != value)
				{
					_movingUp = value;
					UpdateForceVector();
					UpdatePlayerMoveState();
				}
			}
		}

		protected bool _movingDown = false;
		public bool MovingDown
		{
			get
			{
				return _movingDown;
			}
			set
			{
				if ((_movingDown != value) && CanMoveDown)
				{
					_movingDown = value;
					UpdateForceVector();
					UpdatePlayerMoveState();
				}
			}
		}

		protected bool _canFly;
		public bool CanFly
		{
			get
			{
				return _canFly;
			}
			set
			{
				_canFly = value;
				UpdateForceVector();
			}
		}

		protected Vector2 CurrentForceVector = new Vector2(0, 0);

		protected JumpingData CurrentJumpingData//all data and methods for jumping information
		{
			get;
			set;
		}

		public bool CanMoveDown//Can player force himself down? In future, remake it to support squatting
		{
			get;
			set;
		}

		public bool CanMoveInFalling//can player manipulate personage in falling?
		{
			get;
			set;
		}

		public bool IsOnGround//Get current value IsOnGround. If you want update it, use UpdateIsOnGround()
		{
			get;
			protected set;
		}

		public MoveController()
		{

		}

		public MoveController(GameObject playerObject, Rigidbody2D player_rb, Animator player_animator)
		{
			this.Rigidbody2D = player_rb;
			this.Transform = playerObject.transform;
			this.Collider2D = playerObject.GetComponent<Collider2D>();
			this.Animator = player_animator;
		}

		public MoveController(MoveController moveController)
		{
			this.Rigidbody2D = moveController.Rigidbody2D;
			this.Transform = moveController.Transform;
			this.Collider2D = moveController.Collider2D;
			this.Animator = moveController.Animator;
			
			this.MovingDown = moveController.MovingDown;
			this.MovingLeft = moveController.MovingLeft;
			this.MovingUp = moveController.MovingUp;
			this.MovingRight = moveController.MovingRight;
		}

		public abstract void Jump();

		public virtual void ReInit()
		{
			UpdateIsOnGround();
			UpdatePlayerMoveState();
		}

		public abstract void FixedTick();//call this method in every frame. Please, call it in 'FixedUpdate', not in 'Update'.

		protected virtual void UpdateForceVector()
		{
			CurrentForceVector = new Vector2(0, 0);

			if (MovingRight)
				CurrentForceVector.x += 1;

			if (MovingLeft)
				CurrentForceVector.x += -1;

			if (MovingUp)
			{
				if (CanFly)
					CurrentForceVector.y += 1;
			}

			if (MovingDown)
			{
				if (CanMoveDown)
				{
					CurrentForceVector.y += -1;
				}
			}
		}

		protected virtual void UpdateScale()
		{
			if (CurrentForceVector.x == 0)
				return;
			Transform.localScale = new Vector3(
				Math.Abs(Transform.localScale.x) * CurrentForceVector.x,
				Transform.localScale.y,
				Transform.localScale.z);
		}

		protected abstract void UpdatePlayerMoveState();		

		public virtual void UpdateIsOnGround()//Updates current value of IsOnGround.
		{
			//Берем 3 точки, все в самом низу коллайдера по Y, и слева, по центру, справа по иксу. Ну дальше дело техники.
			bool ray_left = Physics2D.Raycast(
				new Vector2(Collider2D.bounds.min.x, Collider2D.bounds.min.y - 0.1f),
				new Vector2(0, -1), 0.1f);

			bool ray_middle = Physics2D.Raycast(
				new Vector2(Collider2D.bounds.center.x, Collider2D.bounds.min.y - 0.1f),
				new Vector2(0, -1), 0.1f);

			bool ray_right = Physics2D.Raycast(
				new Vector2(Collider2D.bounds.max.x, Collider2D.bounds.min.y - 0.1f),
				new Vector2(0, -1), 0.1f);

			bool nowGrounded = ray_left || ray_middle || ray_right;

			if (nowGrounded != IsOnGround)
			{
				IsOnGround = nowGrounded;

				Animator.SetBool("IsFalling", !IsOnGround);
			}
		}
	}
}
