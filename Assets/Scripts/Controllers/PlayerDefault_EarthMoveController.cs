﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

using GamePattern.Helpers;
using GamePattern.Interfaces;
using GamePattern.Controllers;
using GamePattern.MovementArchitecture;

namespace GamePattern.Controllers
{
	public class PlayerDefault_EarthMoveController : EarthMoveController
	{
		public PlayerDefault_EarthMoveController(GameObject playerObject, Rigidbody2D player_rb, Animator player_animator)
			: base(playerObject, player_rb, player_animator)//Call parent constructor
		{
			/*По идее, родительский класс уже имеет весь необходимый функционал для организации движения,
			 но я на будущее решил все таки оставить это как напоминание о том, что контроллеры движения в будущем неминуемо
			 будут развиваться дальше, и придется создавать более архитектуру структуру наследования. Но не думаю что в этом будет
			 необходимость ближайшие месяца 3 (сейчас 26.01.2019)*/
		}

		public PlayerDefault_EarthMoveController(MoveController moveController) : base(moveController)
		{

		}
	}
}
