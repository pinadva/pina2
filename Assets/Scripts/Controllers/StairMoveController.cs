﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

using GamePattern.Helpers;
using GamePattern.Interfaces;
using GamePattern.MovementArchitecture;

public enum StairEndCause { HIGHPOINT, LOWPOINT, PLAYEREND, DESTROYED }

namespace GamePattern.Controllers
{
	public class StairMoveController : MoveController
	{
		public Action<StairEndCause> OnDisconnectFromStair;

		public Vector2 HighPosition;

		public Vector2 LowestPosition;

		public Vector2 CenterPosition;

		protected IForcer StairForcer
		{
			get;
			set;
		}

		public StairMoveController(GameObject playerObject, Rigidbody2D player_rb, Animator player_animator, Bounds stairBounds)
			: base(playerObject, player_rb, player_animator)
		{
			this.HighPosition = new Vector2(stairBounds.center.x, stairBounds.max.y);
			this.LowestPosition = new Vector2(stairBounds.center.x, stairBounds.min.y);
			this.CenterPosition = stairBounds.center;

			this.Transform = playerObject.transform;
			this.Collider2D = playerObject.GetComponent<Collider2D>();

			this.Rigidbody2D = player_rb;
			player_rb.velocity = new Vector2(0, 0);

			this.Animator = player_animator;

			this.CurrentJumpingData = new JumpingData(false, true, 0, 500);

			this.CanMoveDown = true;
			this.CanMoveInFalling = true;
			this.CanFly = true;

			StairForcer = new ConstantForcer(player_rb, 3);
		}

		public StairMoveController(MoveController moveController, Bounds stairBounds)
			: base(moveController)
		{
			this.HighPosition = new Vector2(stairBounds.center.x, stairBounds.max.y);
			this.LowestPosition = new Vector2(stairBounds.center.x, stairBounds.min.y);
			this.CenterPosition = stairBounds.center;

			Rigidbody2D.velocity = new Vector2(0, 0);

			this.CurrentJumpingData = new JumpingData(false, true, 0, 500);

			this.CanMoveDown = true;
			this.CanMoveInFalling = true;
			this.CanFly = true;

			StairForcer = new ConstantForcer(Rigidbody2D, 3);
		}

		public override void Jump()
		{
			//This controller have no functions for jumping
		}

		public override void FixedTick()
		{
			if(CurrentForceVector.y > 0)
			{
				var differentVector = HighPosition - Rigidbody2D.position;

				if (differentVector.magnitude < 2)
				{
					if(OnDisconnectFromStair != null)
					{
						OnDisconnectFromStair(StairEndCause.HIGHPOINT);
					}
					return;
				}

				Vector3 forcingVector = Vector3.Normalize(differentVector);

				StairForcer.Force(forcingVector);
			}
			else if (CurrentForceVector.y < 0)
			{
				var differentVector = LowestPosition - Rigidbody2D.position;

				UpdateIsOnGround();

				if (IsOnGround || differentVector.magnitude < 2)
				{
					OnDisconnectFromStair(StairEndCause.LOWPOINT);
				}

				Vector3 forcingVector = Vector3.Normalize(differentVector);
				StairForcer.Force(forcingVector);
			}
		}

		public override void UpdateIsOnGround()
		{
			bool ray_left = Physics2D.Raycast(
				new Vector2(Collider2D.bounds.min.x, Collider2D.bounds.min.y - 0.1f),
				new Vector2(0, -1), 0.1f);

			bool ray_middle = Physics2D.Raycast(
				new Vector2(Collider2D.bounds.center.x, Collider2D.bounds.min.y - 0.1f),
				new Vector2(0, -1), 0.1f);

			bool ray_right = Physics2D.Raycast(
				new Vector2(Collider2D.bounds.max.x, Collider2D.bounds.min.y - 0.1f),
				new Vector2(0, -1), 0.1f);

			bool nowGrounded = ray_left || ray_middle || ray_right;

			if (nowGrounded != IsOnGround)
			{
				IsOnGround = nowGrounded;

				Animator.SetBool("IsFalling", !IsOnGround);
			}
		}

		protected override void UpdatePlayerMoveState()
		{
			UpdateIsOnGround();

			if (IsOnGround)
			{
				Animator.SetBool("IsFalling", false);
			}

			if (CurrentForceVector.x != 0)
			{

				Animator.SetBool("IsRunning", true);
			}
			else
			{
				Animator.SetBool("IsRunning", false);
			}

			UpdateScale();
		}

	}
}