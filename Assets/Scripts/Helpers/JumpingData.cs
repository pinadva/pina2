﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GamePattern.Helpers
{
	public class JumpingData
	{
		public JumpingData(bool canJump, bool jumpHasCooldown, int jumpForce, int jumpCooldown_msec)
		{
			this.CanJump = canJump;
			this.JumpHasCooldown = jumpHasCooldown;
			this.JumpForce = jumpForce;
			this.JumpСooldown_msec = jumpCooldown_msec;
		}

		public bool CanJump
		{
			get;
			set;
		}

		public bool JumpHasCooldown
		{
			get;
			set;
		}

		public int JumpForce
		{
			get;
			set;
		}

		public int JumpСooldown_msec
		{
			get;
			set;
		}

		public DateTime OldJumpTime
		{
			get;
			set;
		}

		public bool JumpIsReady()
		{
			double different = (DateTime.Now - OldJumpTime).TotalMilliseconds;
			return different > JumpСooldown_msec;
		}
	}
}
