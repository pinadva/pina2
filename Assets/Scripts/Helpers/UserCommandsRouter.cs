﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;
using GamePattern.Controllers;
using GamePattern.Interfaces;


namespace GamePattern.Helpers
{
	public enum KeyEventType
	{
		DOWN = 0,
		PRESSED,
		UP
	}

	public enum MoveAction
	{
		PLAYER_MOVE_LEFT = 1,
		PLAYER_MOVE_UP,
		PLAYER_MOVE_RIGHT,
		PLAYER_MOVE_DOWN,
		PLAYER_JUMP
	}

	public class UserCommandsRouter
	{
		public Dictionary<KeyCode, Action> KeyDownDictionary = new Dictionary<KeyCode, Action>();
		public Dictionary<KeyCode, Action> KeyPressedDictionary = new Dictionary<KeyCode, Action>();
		public Dictionary<KeyCode, Action> KeyUpDictionary = new Dictionary<KeyCode, Action>();

		public void InvokeKeyDown(KeyCode keyCode)
		{
			Action invoking = KeyDownDictionary[keyCode];
			if (invoking != null)
			{
				KeyDownDictionary[keyCode].Invoke();
			}
		}

		public void InvokeKeyPressed(KeyCode keyCode)
		{
			Action invoking = KeyPressedDictionary[keyCode];
			if (invoking != null)
			{
				KeyPressedDictionary[keyCode].Invoke();
			}
		}

		public void InvokeKeyUp(KeyCode keyCode)
		{
			Action invoking = KeyUpDictionary[keyCode];
			if (invoking != null)
			{
				KeyUpDictionary[keyCode].Invoke();
			}
		}

		public void RegisterKey(KeyCode keyCode, Action onInvoke, KeyEventType eventType)
		{
			switch(eventType)
			{
				case (KeyEventType.DOWN):
					{
						KeyDownDictionary.Add(keyCode, onInvoke);
						break;
					}
				case (KeyEventType.PRESSED):
					{
						KeyPressedDictionary.Add(keyCode, onInvoke);
						break;
					}
				case (KeyEventType.UP):
					{
						KeyUpDictionary.Add(keyCode, onInvoke);
						break;
					}
			}
		}

		public void RegisterMoveKey(KeyCode keyCode, MoveAction actionIndex)
		{
			Action onDown = null;

			Action onUp = null;

			switch (actionIndex)
			{
				case (MoveAction.PLAYER_MOVE_LEFT):
					{
						onDown = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingLeft = true;
							}
						};

						onUp = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingLeft = false;
							}
						};

						break;
					}
				case (MoveAction.PLAYER_MOVE_RIGHT):
					{
						onDown = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingRight = true;
							}
						};

						onUp = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingRight = false;
							}
						};

						break;
					}
				case (MoveAction.PLAYER_MOVE_UP):
					{
						onDown = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingUp = true;
							}
						};

						onUp = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingUp = false;
							}
						};

						break;
					}
				case (MoveAction.PLAYER_MOVE_DOWN):
					{
						onDown = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingDown = true;
							}
						};

						onUp = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.MovingDown = false;
							}
						};

						break;
					}
				case (MoveAction.PLAYER_JUMP):
					{
						onDown = () =>
						{
							if (GameController.currentPlayer != null)
							{
								GameController.currentPlayer.MoveController.Jump();
							}
						};

						break;
					}
			}

			KeyDownDictionary[keyCode] = onDown;
			KeyUpDictionary[keyCode] = onUp;
		}
	}
}