﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;
using GamePattern.Controllers;
using GamePattern.Interfaces;

namespace GamePattern.Interfaces
{
	public interface IControllable
	{
		MoveController MoveController
		{
			get;
			set;
		}
	}
}
