﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GamePattern.Interfaces
{
	public interface IDamagable
	{
		HealthManager HealthManager
		{
			get;
			set;
		}
	}

	public class HealthManager
	{
		public Action onDeath;
		public Action<int> onDamage;
		public Action<int> healthChanged;

		//public List<Action> onDeath;
		//public List<Action> onDamage;

		public int maxHealth = 100;
		public int currentHealth = 100;

		public void Damadge(int dmg)
		{
			if (onDamage != null)
			{
				onDamage.Invoke(dmg);
			}

			int newHealth = currentHealth - dmg;

			if (newHealth <= 0)
			{
				Kill();
			}
			else
			{
				currentHealth = newHealth;
			}
			healthChanged(newHealth);
		}

		public void Kill()
		{
			if (onDeath!=null)
			{
				onDeath.Invoke();
			}

		}

	}

}
