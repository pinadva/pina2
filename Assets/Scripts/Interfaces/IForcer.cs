﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

using GamePattern.Helpers;
using GamePattern.Interfaces;

namespace GamePattern.Interfaces
{
	public interface IForcer
	{
		Rigidbody2D workingRigidbody
		{
			get;
			set;
		}

		void Force(Vector2 vect);
	}
}
