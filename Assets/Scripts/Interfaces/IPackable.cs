﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GamePattern.Interfaces
{
	public interface IPackable
	{
		void Pack();

		void UnPack();

		void ShowInfo();

		void Select();
	}
}
