﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace GamePattern.Interfaces
{
	public interface IPlayerOpportunity
	{		
		PlayerController currentPlayer
		{
			get;
			set;
		}

		GameObject Sender
		{
			get;
			set;
		}

		string Description
		{
			get;
			set;
		}

		void Invoke();

		void OnBeginContact();

		void OnEndContact();
	}
}
