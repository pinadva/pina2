﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

using GamePattern.Helpers;
using GamePattern.Interfaces;

namespace GamePattern.MovementArchitecture
{
	public class ConstantForcer : IForcer
	{
		public float ForcingPerFrame;

		public Rigidbody2D workingRigidbody
		{
			get;
			set;
		}
		public ConstantForcer(Rigidbody2D workingRigidbody, int speed)
		{
			this.workingRigidbody = workingRigidbody;
			this.ForcingPerFrame = speed;
		}
		public void Force(Vector2 forcing)
		{
			workingRigidbody.MovePosition(workingRigidbody.position + forcing * ForcingPerFrame);
		}
	}
}
