﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;

using GamePattern.Helpers;
using GamePattern.Interfaces;

namespace GamePattern.MovementArchitecture
{
	public class ImpulseForcer : IForcer
	{
		public Rigidbody2D workingRigidbody
		{
			get;
			set;
		}

		public int OneTimeForcing
		{
			get;
			set;
		}

		public int MaxSpeed
		{
			get;
			set;
		}


		public ImpulseForcer(Rigidbody2D rigidbody2D, int oneTimeForcing = 70000, int maxSpeed = 200)
		{
			this.workingRigidbody = rigidbody2D;
			this.OneTimeForcing = oneTimeForcing;
			this.MaxSpeed = maxSpeed;
		}

		public void Force(Vector2 vect)
		{
			if (Math.Abs(workingRigidbody.velocity.x) < MaxSpeed)
			{
				workingRigidbody.AddForce(vect * OneTimeForcing);
			}
		}
	}
}
