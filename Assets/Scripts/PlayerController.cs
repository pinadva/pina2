﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using UnityEngine;
using GamePattern.Controllers;
using GamePattern.Interfaces;
using GamePattern.MovementArchitecture;

public class BackPackController
{
	public event Action PackCallback;

	public event Action DropCallback;

	public List<GameObject> BackPackItems
	{
		get;
		private set;
	}

	public BackPackController()
	{
		BackPackItems = new List<GameObject>();
	}

	public void PackItem(GameObject item)
	{
		if (item == null)
			return;

		item.SetActive(false);

		BackPackItems.Add(item);

		if(PackCallback != null)
			PackCallback.Invoke();
	}

	public void DropItem(GameObject item)
	{
		throw new Exception("Droping game object by exemplar is not released");
	}

	public void DropItem(int index, Vector3 newPosition)
	{
		GameObject dropIt = BackPackItems[index];
		BackPackItems.RemoveAt(0);

		dropIt.transform.position = newPosition;

		dropIt.SetActive(true);

		if (DropCallback != null)
			DropCallback.Invoke();
	}

}

public class PlayerOpportunitiesController
{
	public List<IPlayerOpportunity> playerOpportunities = new List<IPlayerOpportunity>();

	public void InvokeOpportunity(IPlayerOpportunity opportunity)
	{
		throw new Exception();
	}

	public void InvokeOpportunity(int index)
	{
		playerOpportunities[0].Invoke();
	}

	public void AddPlayerOpportunity(IPlayerOpportunity opportunity)
	{
		playerOpportunities.Add(opportunity);
	}

	public void RemoveOpportunitiesFromSender(GameObject sender)
	{
		playerOpportunities.RemoveAll(i =>
		{
			var result = i.Sender == sender;

			if (result)
			{
				i.OnEndContact();
			}

			return result;
		});
	}
}

public class PlayerController : MonoBehaviour, IControllable, IDamagable
{
	public BackPackController BackPack = new BackPackController();

	public PlayerOpportunitiesController playerOpportunities = new PlayerOpportunitiesController();

	public MoveController MoveController
	{
		get;
		set;
	}

	public HealthManager HealthManager
	{
		get;
		set;
	}

	public GameObject playerObject;
	public Rigidbody2D player_rb;
	public Animator player_animator;

	void Awake()
	{
		this.HealthManager = new HealthManager()
		{
			maxHealth = 100,
			currentHealth = 100
		};

		player_rb = playerObject.GetComponent<Rigidbody2D>();
		player_animator = playerObject.GetComponent<Animator>();

		this.MoveController = new PlayerDefault_EarthMoveController(this.gameObject, this.player_rb, this.player_animator);
	}

	void Start()
	{
		
	}

	void Update()
	{

	}

	void FixedUpdate()
	{
		MoveController.FixedTick();
	}

	void OnCollisionEnter2D()
	{
		MoveController.UpdateIsOnGround();
	}

	void OnCollisionExit2D()
	{
		MoveController.UpdateIsOnGround();
	}
}
