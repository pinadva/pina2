﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using GamePattern.Interfaces;

public class PackingOpportunity : IPlayerOpportunity
{
	public PlayerController currentPlayer
	{
		get;
		set;
	}

	public GameObject Sender
	{
		get;
		set;
	}

	public string Description
	{
		get;
		set;
	}

	public PackingOpportunity(GameObject sender, PlayerController targetPlayer)
	{
		this.Sender = sender;
		this.currentPlayer = targetPlayer;

		this.Description = "Press binded key to pick up item";
	}

	public void Invoke()
	{
		currentPlayer.BackPack.PackItem(Sender);
	}

	public void OnBeginContact()
	{

	}

	public void OnEndContact()
	{

	}
}

public class TackingBall : MonoBehaviour, IPackable
{
	void OnTriggerEnter2D(Collider2D collision)
	{
		PlayerController found = Resources.FindObjectsOfTypeAll<PlayerController>().
			FirstOrDefault(i => i.GetComponent<Collider2D>() == collision);

		if (found == null)
			return;

		var opportunity = new PackingOpportunity(this.gameObject, found);

		found.playerOpportunities.AddPlayerOpportunity(opportunity);
		opportunity.OnBeginContact();
	}

	void OnTriggerExit2D(Collider2D collision)
	{
		PlayerController found = Resources.FindObjectsOfTypeAll<PlayerController>().
			FirstOrDefault(i => i.GetComponent<Collider2D>() == collision);

		if (found == null)
			return;

		found.playerOpportunities.RemoveOpportunitiesFromSender(this.gameObject);		
	}

	public void ShowInfo()
	{
		Debug.Log("Trying show info!");
	}

	public void Select()
	{
		Debug.Log("Trying select!");
	}

	public void Pack()
	{
		Debug.Log("Trying pack!");
	}

	public void UnPack()
	{
		Debug.Log("Trying unpack!");
	}
}
