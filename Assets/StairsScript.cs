﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using GamePattern.Controllers;
using GamePattern.Interfaces;
using GamePattern.MovementArchitecture;

public class StairsScript : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		PlayerController found = Resources.FindObjectsOfTypeAll<PlayerController>().
			   FirstOrDefault(i => i.GetComponent<Collider2D>() == collision);

		if (found == null)
			return;

		Bounds currentBounds = this.GetComponent<Collider2D>().bounds;

		var opportunity = new StairOpportunity(this.gameObject, found, this.GetComponent<Collider2D>().bounds);

		found.playerOpportunities.AddPlayerOpportunity(opportunity);
		opportunity.OnBeginContact();
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		PlayerController found = Resources.FindObjectsOfTypeAll<PlayerController>().
			FirstOrDefault(i => i.GetComponent<Collider2D>() == collision);

		if (found == null)
			return;

		found.playerOpportunities.RemoveOpportunitiesFromSender(this.gameObject);
	}
}

public class StairOpportunity : IPlayerOpportunity
{
	public Bounds StairBounds;

	public bool IsOnStair = false;

	public PlayerController currentPlayer
	{
		get;
		set;
	}

	public GameObject Sender
	{
		get;
		set;
	}

	public string Description
	{
		get;
		set;
	}

	private float oldGravityScale = -1;

	public StairOpportunity(GameObject sender, PlayerController playerController, Bounds stairBounds)
	{
		this.Sender = sender;
		this.currentPlayer = playerController;
		this.StairBounds = stairBounds;
	}

	public void Invoke()
	{
		if (currentPlayer == null)
			throw new System.NullReferenceException("Player is null.");

		IsOnStair = !IsOnStair;

		Debug.Log(IsOnStair ? "Взбираемся..." : "Слезаем с лестницы");

		if (IsOnStair)
		{
			BeginStair();
		}
		else
		{
			EndStair();
		}
	}

	public void OnBeginContact()
	{
		GameController.SetNewPlayerHelpInfo("Press 'E' to go up");
	}

	public void OnEndContact()
	{
		GameController.SetNewPlayerHelpInfo("");

		if(IsOnStair)
		{
			EndStair();
			IsOnStair = !IsOnStair;
		}

	}

	private void BeginStair()
	{
		oldGravityScale = currentPlayer.player_rb.gravityScale;

		currentPlayer.player_rb.bodyType = RigidbodyType2D.Kinematic;

		currentPlayer.transform.position =
			new Vector3(StairBounds.center.x, currentPlayer.transform.position.y, 0);

		currentPlayer.MoveController.Rigidbody2D.gravityScale = 0;

		currentPlayer.MoveController = new StairMoveController(currentPlayer.MoveController, this.StairBounds)
		{
			OnDisconnectFromStair = (i) =>
			{
				Invoke();
				if (i == StairEndCause.HIGHPOINT)
				{
					currentPlayer.player_rb.AddForce(new Vector2(120000 * (currentPlayer.player_rb.transform.localScale.x > 0 ? 1 : -1), 0));
				}	
			}
		};

	}

	private void EndStair()
	{
		currentPlayer.player_rb.gravityScale = oldGravityScale;
		oldGravityScale = -1;
		currentPlayer.player_rb.bodyType = RigidbodyType2D.Dynamic;
		currentPlayer.MoveController = new PlayerDefault_EarthMoveController(currentPlayer.MoveController);
	}
}
